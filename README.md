# 👽 Alien Runner! 👽


## How to play

You'll need **node** installed. Clone this repo, then do:

```shell
# if you have yarn installed
$ yarn global add http-server && yarn install

# otherwise
$ npm i -g http-server && npm install

# to run the dev server
$ npm run dev

# with dashboard
$ npm run dash

# to build for prod
$ npm run prod && cd dist && http-server
```

In all three of these cases (dev, dash, or prod), you can open [http://localhost:8080](http://localhost:8080) to play!
