import Phaser from 'phaser-ce'

import { bootConfig } from 'config'

const Boot = (game) => {


  return {
    preload: () => {

    },
    create: () => {
      game.state.start(bootConfig.nextState)
    }
  }
}

export default Boot
