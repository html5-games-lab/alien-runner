import Phaser from 'phaser-ce'

import { loadConfig } from 'config'

const Load = (game) => {


  return {
    preload: () => {

    },
    create: () => {
      game.state.start(loadConfig.nextState)
    }
  }
}

export default Load
