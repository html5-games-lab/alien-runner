import Phaser from 'phaser-ce'

export const gameConfig = {
  nextState: 'boot',
  assetsPath: 'assets',
  width: 1024,
  height: 640,
  renderer: Phaser.AUTO,
  parent: '',
  state: null,
  transparent: false,
  antialias: true,
  physicsConfig: Phaser.Physics.ARCADE,
  initialGlobal: { score: 0 },
  scoreLabelText: 'Score: '
}

export const bootConfig = {
  nextState: 'load'
}

export const loadConfig = {
  nextState: 'menu'
}

export const menuConfig = {
  nextState: 'play'
}

export const playConfig = {
  nextState: 'menu'
}


